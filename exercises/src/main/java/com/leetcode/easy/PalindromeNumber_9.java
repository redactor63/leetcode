package com.leetcode.easy;

public class PalindromeNumber_9 {

    public static void main(String[] args) {

        System.out.println(new PalindromeNumber_9().isPalindrome(121));
        System.out.println(new PalindromeNumber_9().isPalindrome(1221));
        System.out.println(new PalindromeNumber_9().isPalindrome(-121));
        System.out.println(new PalindromeNumber_9().isPalindrome(-10));

    }

    public boolean isPalindromeMy(int x) {
        if (x < 0) {
            return false;
        }
        if (x < 10) {
            return true;
        }
        int rightDigit = 0;
        int leftDigit = digitsCount(x) - 1;
        while (rightDigit <= leftDigit) {
            if ((x / Double.valueOf(Math.pow(10, rightDigit)).intValue() % 10)
                != (x / Double.valueOf(Math.pow(10, leftDigit)).intValue() % 10)) {
                return false;
            }
            rightDigit++;
            leftDigit--;
        }
        return true;
    }

    private int digitsCount(int number) {
        if (number < 100) {
            return 2;
        } else if (number < 1_000) {
            return 3;
        } else if (number < 10_000) {
            return 4;
        } else if (number < 100_000) {
            return 5;
        } else if (number < 1_000_000) {
            return 6;
        } else if (number < 10_000_000) {
            return 7;
        } else if (number < 100_000_000) {
            return 8;
        } else if (number < 1_000_000_000) {
            return 9;
        }
        return 10;
    }

    public boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        if (x < 10) {
            return true;
        }
        int revertedNumber = 0;
        while(x > revertedNumber) {
            revertedNumber = revertedNumber * 10 + x % 10;
            x /= 10;
        }
        return x == revertedNumber || x == revertedNumber/10;
    }

}

