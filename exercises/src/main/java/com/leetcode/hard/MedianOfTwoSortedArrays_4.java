package com.leetcode.hard;

public class MedianOfTwoSortedArrays_4 {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {

        int nums1Length = nums1.length;
        int nums2Length = nums2.length;

        if (nums1Length > nums2Length) {
            int[] tmp = nums1; nums1 = nums2; nums2 = tmp;
            int temp = nums1Length; nums1Length = nums2Length; nums2Length = temp;
        }

        int iMin = 0, iMax = nums1Length, halfLen = (nums1Length + nums2Length + 1) / 2;

        while (iMin <= iMax) {
            int i = (iMin + iMax) / 2;
            int j = halfLen - i;
            if (i < iMax && nums2[j - 1] > nums1[i]) {
                iMin = i + 1;   // i is too small
            } else if (i > iMin && nums1[i - 1] > nums2[j]) {
                iMax = i - 1;   // i is too big
            } else {    // i is perfect
                int maxLeft = 0;

                if (i == 0) {
                    maxLeft = nums2[j - 1];
                } else if (j == 0) {
                    maxLeft = nums1[i - 1];
                } else {
                    maxLeft = Math.max(nums1[i - 1], nums2[j - 1]);
                }

                if ((nums1Length + nums2Length) % 2 == 1) {
                    return maxLeft;
                }

                int minRight = 0;
                if (i == nums1Length) {
                    minRight = nums2[j];
                } else if (j == nums2Length) {
                    minRight = nums1[i];
                } else {
                    minRight = Math.min(nums2[j], nums1[i]);
                }

                return (maxLeft + minRight) / 2.0;

            }
        }

        return 0.0;

    }

}
