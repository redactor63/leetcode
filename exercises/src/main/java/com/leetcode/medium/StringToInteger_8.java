package com.leetcode.medium;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringToInteger_8 {

    public static void main(String[] args) {
        System.out.println(new StringToInteger_8().myAtoi("+"));
        System.out.println(new StringToInteger_8().myAtoi(" "));
    }

    public int myAtoi(String str) {
        if (str.length() == 0) {
            return 0;
        }
        int pointer = 0;
        while(pointer < str.length() && str.charAt(pointer) == ' ') {
            pointer++;
        }
        if (pointer == str.length()) {
            return 0;
        }
        char charAtPointer = str.charAt(pointer);
        if (((charAtPointer != '-' && charAtPointer != '+') && (charAtPointer < '0' || charAtPointer > '9'))
                || ((charAtPointer == '-' || charAtPointer == '+') && (str.length() == 1 || str.charAt(pointer + 1) < '0' || str.charAt(pointer + 1) > '9'))
        ) {
            return 0;
        }
        int startPointer = pointer;
        while(pointer < str.length() - 1
            && str.charAt(pointer + 1) >= '0'
            && str.charAt(pointer + 1) <= '9'
        ) {
            pointer++;
        }
        charAtPointer = str.charAt(pointer);
        if (charAtPointer < '0' || charAtPointer > '9') {
            return 0;
        }
        try {
            return Integer.parseInt(str.substring(startPointer, pointer + 1));
        } catch (NumberFormatException exc) {
            return (str.charAt(startPointer) == '-' ? Integer.MIN_VALUE : Integer.MAX_VALUE);
        }
    }


}
