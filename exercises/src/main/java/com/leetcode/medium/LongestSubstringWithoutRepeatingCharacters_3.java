package com.leetcode.medium;

public class LongestSubstringWithoutRepeatingCharacters_3 {

    public static void main(String args[]) {
        System.out.println("3 == " + new LongestSubstringWithoutRepeatingCharacters_3().lengthOfLongestSubstring("abba"));
    }

//    my
//    public int lengthOfLongestSubstring(String s) {
//        int maxLength = 0;
//        int length = 0;
//        Set<Character> symbols = new HashSet<>();
//
//        for (int i = 0; i < s.length(); i++) {
//            symbols.clear();
//            length = 0;
//            for (int j = i; j < s.length(); j++) {
//                if (!symbols.contains(s.charAt(j))) {
//                    length++;
//                    symbols.add(s.charAt(j));
//                } else {
//                    break;
//                }
//            }
//            maxLength = Math.max(maxLength, length);
//        }
//
//        return maxLength;
//
//    }

//    naive
//    public int lengthOfLongestSubstring(String s) {
//        int ans = 0;
//        for (int i = 0; i < s.length(); i++) {
//            for (int j = i + 1; j <= s.length(); j++) {
//                if (allUnique(s, i, j)) {
//                    ans = Math.max(ans, j - i);
//                }
//            }
//        }
//        return ans;
//    }
//
//    private boolean allUnique(String s, int begin, int end) {
//        HashSet<Character> characters = new HashSet<>();
//        for (int i = begin; i < end; i++) {
//            if (!characters.add(s.charAt(i))) {
//                return false;
//            }
//        }
//        return true;
//    }

//    sliding window
//    public int lengthOfLongestSubstring(String s) {
//        int maxLength = 0, leftBorder = 0, rightBorder = 0, size = s.length();
//        Set<Character> symbols = new HashSet<>();
//        while (leftBorder < size && rightBorder < size) {
//            if (!symbols.contains(s.charAt(rightBorder))) {
//                symbols.add(s.charAt(rightBorder++));
//                maxLength = Math.max(rightBorder - leftBorder, maxLength);
//            } else {
//                symbols.remove(s.charAt(leftBorder++));
//            }
//        }
//        return maxLength;
//    }

//    optimized sliding window
//    public int lengthOfLongestSubstring(String s) {
//        int n = s.length(), ans = 0;
//        Map<Character, Integer> map = new HashMap<>();
//        for (int rightBorder = 0, leftBorder = 0; rightBorder < n; rightBorder++) {
//            if (map.containsKey(s.charAt(rightBorder))) {
//                leftBorder = Math.max(map.get(s.charAt(rightBorder)), leftBorder);
//            }
//            ans = Math.max(ans, rightBorder - leftBorder + 1);
//            map.put(s.charAt(rightBorder), rightBorder + 1);
//        }
//        return ans;
//    }

//    sliding window with array instead of map
    public int lengthOfLongestSubstring(String s) {
        int n = s.length(), ans = 0;
        int[] index = new int[128]; // 26 for a-z or A-Z, 128 for ASCII, 256 for Extended ASCII
        for (int j = 0, i = 0; j < n; j++) {
            i = Math.max(index[s.charAt(j)], i);
            ans = Math.max(ans, j - i + 1);
            index[s.charAt(j)] = j + 1;
        }
        return ans;
    }



}
