package com.leetcode.medium;


public class LongestPalindromicString_5 {

    public static void main(String[] args) {
        System.out.println(new LongestPalindromicString_5().longestPalindrome("abb"));
        System.out.println(new LongestPalindromicString_5().longestPalindrome("abba"));
        System.out.println(new LongestPalindromicString_5().longestPalindrome("abcdcba"));
    }

//     my solution
//    public String longestPalindrome(String s) {
//        int windowLength = s.length();
//        while (windowLength > 0) {
//
//            int right = 0;
//            int left = windowLength;
//
//            do {
//                System.out.println("Window length: " + windowLength + ". Right: " + right + ". Left: " + left);
//                String substring = s.substring(right++, left++);
//                if (isPalindrome(substring)) {
//                    return substring;
//                }
//            } while (left <= s.length() );
//
//            windowLength--;
//        }
//        return "";
//    }
//
//    private boolean isPalindrome(String s) {
//        for (int i = 0; i < s.length() / 2; i++) {
//            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) {
//                return false;
//            }
//        }
//        return true;
//    }

    public String longestPalindrome(String s) {
        if (s == null || s.length() < 1) {
            return "";
        }
        int start = 0, end = 0;
        for (int i = 0; i < s.length(); i++) {
            int len1 = expandAroundCenter(s, i, i);
            int len2 = expandAroundCenter(s, i, i + 1);
            int len = Math.max(len1, len2);
            if (len > end - start) {
                start = i - (len - 1) / 2;
                end = i + len / 2;
            }
        }
        return s.substring(start, end + 1);
    }

    private int expandAroundCenter(String s, int left, int right) {
        int L = left, R = right;
        while (L >= 0 && R < s.length() && s.charAt(L) == s.charAt(R)) {
            System.out.println(s.substring(L, R + 1));
            L--;
            R++;
        }
        return R - L - 1;
    }

}
