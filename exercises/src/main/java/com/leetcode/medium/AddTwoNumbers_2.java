package com.leetcode.medium;

public class AddTwoNumbers_2 {

    public static void main(String[] args) {

        // 9 + 1
        ListNode _9 = new ListNode(9);
        ListNode _1 = new ListNode(1);
        // 0 - 1
        System.out.println(new Solution().addTwoNumbers(_9, _1));

        // 8 + 1
        ListNode _8 = new ListNode(8);
        // 9
        System.out.println(new Solution().addTwoNumbers(_8, _1));

        // 12 + 34
        ListNode _12 = new ListNode(2, new ListNode(1));
        ListNode _34 = new ListNode(4,new ListNode(3));
        // 6 - 4
        System.out.println(new Solution().addTwoNumbers(_12, _34));

        // 99 + 11
        ListNode _99 = new ListNode(9, new ListNode(9));
        ListNode _11 = new ListNode(1, new ListNode(1));
        // 0 - 1 - 1
        System.out.println(new Solution().addTwoNumbers(_99, _11));

        // 342 + 465
        ListNode _342 = new ListNode(2, new ListNode(4, new ListNode(3)));
        ListNode _465 = new ListNode(5, new ListNode(6, new ListNode(4)));
        // 7 - 0 - 8
        System.out.println(new Solution().addTwoNumbers(_342, _465));

        // 909 + 101
        ListNode _909 = new ListNode(9, new ListNode(0, new ListNode(9)));
        ListNode _101 = new ListNode(1, new ListNode(0, new ListNode(1)));
        // 0 - 1 - 0 - 1
        System.out.println(new Solution().addTwoNumbers(_909, _101));

    }

    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }

        public String toString() {
            StringBuilder s = new StringBuilder("{");
            ListNode current = this;
            do {
                s.append(current.val).append(" -> ");
                current = current.next;
            } while (current != null);
            return s.append("}").toString();
        }

    }

    static class Solution {
        public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

            ListNode previous = null;
            ListNode head = null;
            boolean additional = false;
            ListNode currentL1 = l1;
            ListNode currentL2 = l2;

            do {

                int sum =
                    (currentL1 == null ? 0 : currentL1.val) +
                    (currentL2 == null ? 0 : currentL2.val) +
                    (additional ? 1 : 0);

                additional = (sum >= 10);

                ListNode currentNode = new ListNode(additional ? sum % 10 : sum);
                if (previous != null) {
                    previous.next = currentNode;
                } else {
                    head = currentNode;
                }
                previous = currentNode;

                if (currentL1 != null) {
                    currentL1 = currentL1.next;
                }
                if (currentL2 != null) {
                    currentL2 = currentL2.next;
                }

            } while (currentL1 != null || currentL2 != null || additional);

            return head;

        }

        public ListNode leetcodeSolution(ListNode l1, ListNode l2) {
            ListNode dummyHead = new ListNode(0);
            ListNode p = l1, q = l2, curr = dummyHead;
            int carry = 0;
            while(p != null || q != null) {
                int x = (p != null) ? p.val : 0;
                int y = (q != null) ? q.val : 0;
                int sum = carry + x + y;
                carry = sum / 10;
                curr.next = new ListNode(sum % 10);
                curr = curr.next;
                if (p != null) p = p.next;
                if (q != null) q = q.next;
            }
            if (carry > 0) {
                curr.next = new ListNode(carry);
            }
            return dummyHead.next;
        }


    }

}
