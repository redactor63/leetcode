package com.leetcode.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ZigzagConversion_6 {

    public static void main(String[] args) {

        if (!"ABCDEF".equals(new ZigzagConversion_6().convert("ABCDEF", 1))) {
            throw new RuntimeException();
        }

        if (!"ACEBDF".equals(new ZigzagConversion_6().convert("ABCDEF", 2))) {
            throw new RuntimeException();
        }

        if (!"AEIBDFHCG".equals(new ZigzagConversion_6().convert("ABCDEFGHI", 3))) {
            throw new RuntimeException();
        }

        if (!"AGMBFHLCEIKDJ".equals(new ZigzagConversion_6().convert("ABCDEFGHIJKLM", 4 ))) {
            throw new RuntimeException();
        }

        if (!"AIQBHJPCGKODFLNEM".equals(new ZigzagConversion_6().convert("ABCDEFGHIJKLMNOPQ", 5 ))) {
            throw new RuntimeException();
        }

    }

//    public String convert(String s, int numRows) {
//        if (numRows == 1) {
//            return s;
//        }
//        List<List<Character>> rows = new ArrayList<>();
//        for (int i = 0; i < numRows; i++) {
//            rows.add(new ArrayList<>());
//        }
//        char[] characters = s.toCharArray();
//        int rowIndex = 0;
//        boolean up = true;
//        for (char character : characters) {
//            rows.get(rowIndex).add(character);
//            if (up) {
//                rowIndex++;
//            } else {
//                rowIndex--;
//            }
//            if (rowIndex == -1) {
//                up = true;
//                rowIndex = 1;
//            } else if (rowIndex == numRows) {
//                up = false;
//                rowIndex = numRows - 2;
//            }
//        }
//        String result = "";
//        for (List<Character> characterList : rows) {
//            for (Character character : characterList) {
//                result += character;
//            }
//        }
//        return result;
//    }

    public String convert(String s, int numRows) {

        if (numRows == 1) {
            return s;
        }

        char[] result = new char[s.length()];
        int resultPointer = 0;

        int stepLeft = (numRows - 1) * 2 - 1;
        int stepRight = 0;

        for (int rowIndex = 0; rowIndex < numRows; rowIndex++) {

            int pointerLeft = rowIndex;
            int pointerRight = pointerLeft + (stepLeft == 0 ? stepRight : stepLeft) + 1;

            while (pointerLeft <= s.length() - 1 || pointerRight <= s.length() - 1) {

                if (pointerLeft <= s.length() - 1) {
                    result[resultPointer++] = s.charAt(pointerLeft);
                }
                if (pointerRight <= s.length() - 1) {
                    result[resultPointer++] = s.charAt(pointerRight);
                }

                pointerLeft = pointerRight + (stepRight == 0 ? stepLeft : stepRight) + 1;
                pointerRight = pointerLeft + (stepLeft == 0 ? stepRight : stepLeft) + 1;

            }

            if (stepLeft == 1) {
                stepLeft = 0;
            } else {
                stepLeft -= 2;
            }

            if (stepRight == 0) {
                stepRight = 1;
            } else {
                stepRight += 2;
            }

        }

        return new String(result);
    }

}
