package edu.algorithms;

public class BubbleSort {

    public void bubbleSort(long[] arr) {
        int out, in;
        for (out = arr.length - 1; out > 1; out--) {    // внешний цикл (обратный)
            for (in = 0; in < out; in++) {              // внутренний цикл (прямой)
                if (arr[in] > arr[in + 1]) {            // порядок нарушен?
                    long temp = arr[in];                // поменять местами
                    arr[in] = arr[in + 1];
                    arr[in + 1] = temp;
                }
            }
        }
    }

}
