package edu.algorithms;

public class CyclicQueueWithoutCounter {

    private long[] arr;

    private int size;

    private int front;

    private int rear = -1;

    public CyclicQueueWithoutCounter(int size) {
        this.arr = new long[size + 1];
        this.size = arr.length;
    }

    public static void main(String[] args) {

    }

    public void insert(long l) {
        if (rear == size - 1) {
            rear = -1;
        }
        arr[++rear] = l;
    }

    public long remove() {
        long temp = arr[front++];
        if (front == size) {
            front = 0;
        }
        return temp;
    }

    public long peek() {
        return arr[front];
    }

    public boolean isEmpty() {
        return rear + 1 == front || front + size - 1 == rear;
    }

    public boolean isFull() {
        return rear + 2 == front || front + size - 2 == rear;
    }

    public int size() {
        if (rear >= front) {
            return rear - front + 1;
        } else {
            return size - front + rear + 1;
        }
    }

}
