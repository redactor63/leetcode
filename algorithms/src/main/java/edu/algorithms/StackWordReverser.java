package edu.algorithms;

import java.util.Stack;

public class StackWordReverser {

    public static void main(String[] s) {
        System.out.println(new StackWordReverser().reverseWord("hello_world"));

        System.out.println(new StackWordReverser().checkBrakes("c[d]"));
        System.out.println(new StackWordReverser().checkBrakes("a{b[c]d}e"));
        System.out.println(new StackWordReverser().checkBrakes("a{b(c]d}e"));
        System.out.println(new StackWordReverser().checkBrakes("a[b{c}d]e}"));
        System.out.println(new StackWordReverser().checkBrakes(" a{b(c)"));

    }

    public String reverseWord(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            stack.push(c);
        }
        String result = "";
        while(!stack.empty()) {
            result += stack.pop();
        }
        return result;
    }

    public boolean checkBrakes(String s) {
        Stack<Character> stack = new Stack<>();
        for (char c : s.toCharArray()) {
            if (c == '(' || c  == '[' || c == '{') {
                stack.push(c);
            } else if (c == ')' || c  == ']' || c == '}') {
                if (stack.empty()) {
                    return false;
                }
                Character stackChar = stack.pop();
                if ((c == ')' && stackChar != '(')
                    || (c == ']' && stackChar != '[')
                    || (c == '}' && stackChar != '{')
                ) {
                    return false;
                }
            }
        }
        return stack.empty();
    }

}
