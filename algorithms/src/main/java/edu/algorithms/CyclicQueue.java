package edu.algorithms;

import java.util.Arrays;

public class CyclicQueue {

    private int size;

    private int count;

    private long[] arr;

    private int front;

    private int rear = -1;

    public CyclicQueue(int size) {
        this.size = size;
        this.arr = new long[size];
    }

    public static void main(String[] args) {
        CyclicQueue q = new CyclicQueue(5);
        q.insert(1);
        q.insert(2);
        q.insert(3);
        q.insert(4);
        q.insert(5);
        System.out.println(q);
        System.out.println(q.isFull());
        System.out.println(q.isEmpty());
        System.out.println(q.size());
        System.out.println("---");

        System.out.println(q.remove());
        System.out.println(q);
        System.out.println(q.isFull());
        System.out.println(q.isEmpty());
        System.out.println(q.size());
        System.out.println("---");

        q.insert(100);
        System.out.println(q);
        System.out.println(q.isFull());
        System.out.println(q.isEmpty());
        System.out.println(q.size());
    }

    public void insert(long newElement) {
        if (rear == size - 1) {     // циклический перенос
            rear = -1;
        }
        arr[++rear] = newElement;   // увеличение rear и вставка
        count++;                    // увеличение количества элементов
    }

    public long remove() {
        long temp = arr[front++];   // выборка и увеличение front
        if (front == size) {        // циклический перенос
            front = 0;
        }
        count--;                    // уменьшение количества элементов
        return temp;
    }

    public long peek() {
        return arr[front];
    }

    public boolean isFull() {
        return count == size;
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public int size() {
        return count;
    }

    @Override
    public String toString() {
        return Arrays.toString(arr) + " front: " + front + ", rear: " + rear;
    }

}
