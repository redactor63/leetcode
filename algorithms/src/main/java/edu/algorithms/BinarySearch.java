package edu.algorithms;

public class BinarySearch {

    public int search(long[] arr, long searchKey) {

        int lowerBound = 0;
        int upperBound = arr.length - 1;
        int curIn;

        while (true) {
            curIn = (lowerBound + upperBound) / 2;
            if (arr[curIn] == searchKey) {
                return curIn;
            } else if (lowerBound > upperBound) {
                return -1;
            } else {
                if (arr[curIn] < searchKey) {
                    lowerBound = curIn + 1;
                } else {
                    upperBound = curIn - 1;
                }
            }
        }

    }

}
