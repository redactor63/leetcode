package edu.algorithms;

import java.util.Arrays;

public class SelectSort {

    public static void main(String[] args) {
        long[] arr = {1, 5, 4, 9, 7, 4, 3};
        new SelectSort().selectSort(arr);
        System.out.println(Arrays.toString(arr));
    }

    public void selectSort(long[] arr) {
        int in, out;
        for (out = 0; out < arr.length; out++) {        // внешний цикл
            int smallestIndex = out;                    // минимум
            for (in = out; in < arr.length; in++) {     // внутренний цикл
                if (arr[in] < arr[smallestIndex]) {     // если значение min больше
                    smallestIndex = in;                 // значит найден новый минимум
                }
            }
            long temp = arr[out];                       // переставить их местами
            arr[out] = arr[smallestIndex];
            arr[smallestIndex] = temp;
        }
    }

}
