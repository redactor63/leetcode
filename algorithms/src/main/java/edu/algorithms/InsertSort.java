package edu.algorithms;

import java.util.Arrays;

public class InsertSort {

    public static void main(String[] args) {
        long[] arr = new long[]{2, 4, 6, 9, 8, 1, 0, 3, 7, 5};
        new InsertSort().sort(arr);
        System.out.println("---");
        System.out.println(Arrays.toString(arr));

        arr = new long[]{77, 99, 44, 55, 22, 88, 11, 0, 66, 33};
        new InsertSort().sort(arr);
        System.out.println("---");
        System.out.println(Arrays.toString(arr));
    }

//    my
//    public void sort(long[] arr) {
//        for (int marker = 1; marker < arr.length; marker++) {
//            long current = arr[marker];
//            int left = marker - 1;
//            int emptyPlaceIndex = marker;
//            while(left >= 0 && arr[left] > current) {
//                long temp = arr[emptyPlaceIndex];
//                arr[emptyPlaceIndex] = arr[left];
//                arr[left] = temp;
//                left--;
//                emptyPlaceIndex--;
//            }
//        }
//    }

//    my2
    public void sort(long[] arr) {
        for (int marker = 1; marker < arr.length; marker++) {
            long current = arr[marker];
            int emptyPlaceIndex = marker;
            while(emptyPlaceIndex > 0 && arr[emptyPlaceIndex - 1] > current) {
                arr[emptyPlaceIndex] = arr[emptyPlaceIndex - 1];
                emptyPlaceIndex--;
            }
            arr[emptyPlaceIndex] = current;
        }
    }

//    public void sort(long[] arr) {
//        for (int out = 1; out < arr.length; out++) {    // out - разделительный маркер
//            long temp = arr[out];                       // скопировать помеченный элементы
//            int in = out;                               // начать с перемещения out
//            while(in > 0 && arr[in - 1] >= temp) {      // пока не найден меньший элемент
//                arr[in] = arr[in - 1];                  // сдвинуть элемент вправо
//                in--;                                   //  перейти на одну позицию влево
//            }
//            arr[in] = temp;                             // вставить помеченный элемент
//        }
//    }

}
