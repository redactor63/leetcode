package edu.algorithms;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MergeSort {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(new MergeSort().sort(new long[]{1, 6, 4, 5, 2, 9, 7, 8, 0})));
        System.out.println();
        System.out.println(Arrays.toString(new MergeSort().sort(new long[]{1, 6, 4, 5, 2, 9, 7, 8, 0, 3})));
    }

    public long[] sort(long[] source) {
        List<long[]> twoParts = divide(source);
        return merge(Arrays.asList(
            twoParts.get(0).length == 1 ? twoParts.get(0) : sort(twoParts.get(0)),
            twoParts.get(1).length == 1 ? twoParts.get(1) : sort(twoParts.get(1))
        ));
    }

    public long[] merge(List<long[]> arrays) {
        arrays.forEach(arr ->
            System.out.print(Arrays.toString(arr) + " + ")
        );
        System.out.println();
        if (arrays.size() == 1) {
            return arrays.get(0);
        }
        long[] result = new long[arrays.get(0).length + arrays.get(1).length];
        int resultPointer = 0;
        int firstArrayPointer = 0;
        int secondArrayPointer = 0;
        while (resultPointer < result.length) {
            if (firstArrayPointer >= arrays.get(0).length) {
                result[resultPointer++] = arrays.get(1)[secondArrayPointer++];
            } else if (secondArrayPointer >= arrays.get(1).length) {
                result[resultPointer++] = arrays.get(0)[firstArrayPointer++];
            } else if (arrays.get(0)[firstArrayPointer] <  arrays.get(1)[secondArrayPointer]) {
                result[resultPointer++] = arrays.get(0)[firstArrayPointer++];
            } else {
                result[resultPointer++] = arrays.get(1)[secondArrayPointer++];
            }
        }
        return result;
    }


    public List<long[]> divide(long[] source) {
        if (source.length == 1) {
            return Collections.singletonList(source);
        }
        int border = source.length / 2;
        return Arrays.asList(
            Arrays.copyOfRange(source, 0, border),
            Arrays.copyOfRange(source, border, source.length)
        );
    }

}
